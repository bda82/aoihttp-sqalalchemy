# Database

```
sudo docker volume create postgres-data

sudo docker run -e POSTGRES_PASSWORD=password -e POSTGRES_USER=postgres -e POSTGRES_DB=aiohttp -p 5432:5432 --name postgres --mount source=postgres-data,target=/var/lib/postgresql -d postgres:12
```


# Alembic

### Init

```
alembic init -t async alembic
```

### Generate new migration file
```
alembic revision --autogenerate -m "Add Tutorial model"
```

### Apply the migration
```
$ alembic upgrade head
```