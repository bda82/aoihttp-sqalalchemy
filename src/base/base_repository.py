from abc import ABC
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError


class BaseRepository(ABC):
    _model: NotImplemented

    def __init__(self, session) -> None:
        self._session = session
        self._name = self._model.__name__

    def mutate(self, **kwargs):
        """
        Can be redefined for custom logic
        """
        return kwargs

    async def get_one(self, todo_id):
        async with self._session as session:
            async with session.begin():
                stmt = select(self._model)
                stmt = stmt.where(self._model.id == todo_id)
                obj = await session.execute(stmt)
                if not obj:
                    raise Exception("Object does not exists")
                for o in obj.scalars():
                    return o

    async def list(self):
        async with self._session as session:
            async with session.begin():
                stmt = select(self._model)
                result = await session.execute(stmt)
                return [r for r in result.scalars()]

    async def create_obj(self, kwargs):
        data = self.mutate(**kwargs)
        obj = self._model(**data)
        async with self._session as session:
            async with session.begin():
                session.add(obj)
                try:
                    session.commit()
                except IntegrityError as e:
                    session.rollback()
                    raise e
                session.refresh(obj)
                return obj
