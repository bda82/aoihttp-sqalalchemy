from aiohttp import web
from aiohttp_cors import CorsViewMixin


class IndexView(web.View, CorsViewMixin):
    async def get(self):
        data = {"message": "OK"}
        status_code = web.HTTPOk().status_code
        return web.json_response(data=data, status=status_code)
