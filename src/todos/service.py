from dataclasses import dataclass
from factories.db_factory import get_session
from base.base_service import BaseService
from todos.repository import TodoRepository


@dataclass
class TodoService(BaseService):
    _todo_repo: TodoRepository

    async def get_all(self):
        return await self._todo_repo.get_all()

    async def create(self, data):
        return await self._todo_repo.create(data)

    async def get(self, todo_id):
        return await self._todo_repo.get(todo_id)

def get_todo_service(db=get_session()) -> TodoService:
    return TodoService(_todo_repo=TodoRepository(db))
