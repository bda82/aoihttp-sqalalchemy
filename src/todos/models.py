from sqlalchemy import Column, Integer, Boolean, String
from sqlalchemy.dialects.postgresql import UUID
from factories.db_factory import BaseModel
from utils.uid import get_random_uuid


class Todo(BaseModel):
    __tablename__ = "todos"

    id = Column(
        UUID(as_uuid=False), primary_key=True, default=get_random_uuid, index=True
    )
    title = Column(String(255), nullable=False)
    description = Column(String(255), nullable=False)
    completed = Column(Boolean)
