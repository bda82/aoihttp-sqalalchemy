from json import JSONDecodeError

from aiohttp import web
from aiohttp_cors import CorsViewMixin
from factories.db_factory import get_session
from todos.schemas import todo_response_schema, todo_create_request_schema
from todos.service import get_todo_service
from aiohttp_apispec import docs, request_schema, response_schema


class TodoListView(web.View, CorsViewMixin):

    @docs(
        tags=["aiohttp"],
        summary="Summary",
        description="Description"
    )
    @response_schema(todo_response_schema, 200)
    async def get(self) -> web.Response:
        todo_service = get_todo_service(get_session())
        items = await todo_service.get_all()
        data = {"items": []}
        for item in items:
            a = todo_response_schema().dump(obj=item)
            data["items"].append(
                a
            )
        status_code = web.HTTPOk().status_code
        return web.json_response(data=data, status=status_code)

    @docs(
        tags=["aiohttp"],
        summary="Summary",
        description="Description"
    )
    @request_schema(todo_create_request_schema)
    @response_schema(todo_response_schema)
    async def post(self):
        try:
            request = await self.request.json()
            if not request:
                raise web.HTTPBadRequest()
        except JSONDecodeError:
            raise web.HTTPBadRequest()
        todo_service = get_todo_service(get_session())
        obj = await todo_service.create(request)
        item = todo_response_schema().dump(obj)
        status_code = web.HTTPOk().status_code
        return web.json_response(data=item, status=status_code)


class TodoRetreiveView(web.View, CorsViewMixin):

    @docs(
        tags=["aiohttp"],
        summary="Summary",
        description="Description"
    )
    async def get(self):
        todo_service = get_todo_service(get_session())
        obj = await todo_service.get(self.request.match_info.get("todo_id"))
        item = todo_response_schema().dump(obj)
        status_code = web.HTTPCreated().status_code
        return web.json_response(data=item, status=status_code)