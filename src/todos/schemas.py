from dataclasses import dataclass, field
from typing import List, Union
from uuid import UUID

import marshmallow_dataclass as mdc
from marshmallow import Schema


@dataclass
class TodoCoreSchema(Schema):
    title: str
    description: str
    completed: bool


@dataclass
class TodoCreateRequestSchema(TodoCoreSchema):
    pass


@dataclass
class TodoResponseSchema(TodoCoreSchema):
    id: str


@dataclass
class TodoResponseListSchema(Schema):
    items: List[TodoResponseSchema] = list


todo_core_schema = mdc.class_schema(TodoCoreSchema)
todo_create_request_schema = mdc.class_schema(TodoCreateRequestSchema)
todo_response_schema = mdc.class_schema(TodoResponseSchema)
todo_response_list_schema = mdc.class_schema(TodoResponseListSchema)
