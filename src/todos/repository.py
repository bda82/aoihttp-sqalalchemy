from base.base_repository import BaseRepository
from todos.models import Todo


class TodoRepository(BaseRepository):
    _model: Todo = Todo

    async def get_all(self):
        return await self.list()

    async def create(self, data):
        return await self.create_obj(data)

    async def get(self, todo_id):
        return await self.get_one(todo_id)