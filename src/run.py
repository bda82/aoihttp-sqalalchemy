from aiohttp import web

from factories.app_factory import app_factory


def init_app():
    app = app_factory()
    return app


def main():
    app = init_app()
    web.run_app(app)


if __name__ == "__main__":
    main()
