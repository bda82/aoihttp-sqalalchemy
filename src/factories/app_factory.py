import asyncio
from aiohttp import web
import aiohttp_cors as aiohttp_cors
from aiohttp_apispec import setup_aiohttp_apispec

from factories.router_factory import router_factory


async def on_startup(app: web.Application = None):
    await asyncio.sleep(0.01)


def app_factory() -> web.Application:
    app = web.Application()

    router_factory(app)

    cors = aiohttp_cors.setup(
        app,
        defaults={
            "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
                max_age=3600,
            )
        },
    )

    for route in list(app.router.routes()):
        cors.add(route)

    setup_aiohttp_apispec(
        app=app,
        title="Documentation",
        version="v0.1",
        url="/api/docs/swagger.json",
        swagger_path="/api/docs",
    )

    app.on_startup.append(on_startup)

    return app
