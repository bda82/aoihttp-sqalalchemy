import os
from datetime import datetime
from contextlib import asynccontextmanager

import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.ext.asyncio import create_async_engine

from sqlalchemy import Column, DateTime
from sqlalchemy.orm import declared_attr

metadata = sa.MetaData()
BaseModel = orm.declarative_base(metadata=metadata)


class DatedModel(object):
    @declared_attr
    def created_at(cls):
        return Column(DateTime, default=datetime.utcnow, index=True)

    @declared_attr
    def updated_at(cls):
        return Column(DateTime, default=datetime.utcnow, index=True)


db_username = os.environ.get("POSTGRES_USER", "postgres")
db_secret = os.environ.get("POSTGRES_PASSWORD", "password")
db_host = os.environ.get("DB_HOST", "localhost")
db_port = os.environ.get("DB_PORT", 5432)
db_name = os.environ.get("POSTGRES_DB", "aiohttp")


connection_string = (
    f"postgresql+asyncpg://{db_username}:{db_secret}@{db_host}:{db_port}/{db_name}"
)


engine = create_async_engine(
    connection_string,
    echo=True,
    future=True,
)


def async_session_generator():
    return sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


@asynccontextmanager
async def get_session():
    """
    async def example(model: Model) -> Model:
        async with get_session() as session:
            ...
            await session.commit()
            ...
    """
    try:
        async_session = async_session_generator()

        async with async_session() as session:
            yield session
    except:
        await session.rollback()
        raise
    finally:
        await session.close()
