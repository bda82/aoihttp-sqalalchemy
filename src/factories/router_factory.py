from aiohttp import web
from index.index import IndexView
from todos.views import TodoListView, TodoRetreiveView


def router_factory(app: web.Application):
    app.router.add_view("", IndexView)
    app.router.add_view("/todos", TodoListView)
    app.router.add_view("/todos/{todo_id}", TodoRetreiveView)
