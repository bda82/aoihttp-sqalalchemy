import uuid


def get_random_uuid(as_str=True):
    res = uuid.uuid4()
    if as_str:
        return str(res)
    return res
